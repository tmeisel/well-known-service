package handlers

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/importer"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/types"
)

func WellKnownCredentialIssuerHandler(c *gin.Context, imp importer.Importer, env *common.Environment) {
	tenantId := c.Param("tenantId")

	if tenantId == "" {
		c.JSON(404, "Not found.")
	}

	template := types.TemplateData{Origin: c.Request.Host + "/" + tenantId, TenantId: tenantId}
	metadata, err := imp.GetCredentialIssuerMetadata(c, template, env)

	if err != nil {
		status := http.StatusInternalServerError
		if errors.Is(err, importer.ErrNotFound) {
			status = http.StatusNotFound
		}

		if err := c.AbortWithError(status, err); err != nil {
			env.GetLogger().Error(err, "failed to write status")
		}
	}

	c.JSON(200, metadata)
}
