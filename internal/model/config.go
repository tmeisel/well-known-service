package model

import (
	cfgPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/config"
	postgresPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/db/postgres"
)

type Config struct {
	cfgPkg.BaseConfig `mapstructure:",squash"`

	Postgres postgresPkg.Config `mapstructure:"postgres"`

	ServingPort    string `mapstructure:"servingPort"`
	Profiles       string `mapstructure:"profiles"`
	LocationHeader string `mapstructure:"locationHeader"`

	CredentialIssuer struct {
		Importer string `mapstructure:"importer"`
	} `mapstructure:"credentialIssuer"`

	Git struct {
		ImagePath string `mapstructure:"imagePath"`
		Repo      string `mapstructure:"repo"`
		Token     string `mapstructure:"token"`
		Interval  int    `mapstructure:"interval"`
	} `mapstructure:"git"`

	JwtIssuer struct {
		Issuer        string `mapstructure:"issuer"`
		JwksUrlHeader string `mapstructure:"jwksUrl"`
	} `mapstructure:"openIdConfiguration"`
}
