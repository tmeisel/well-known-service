package issuers

import (
	"context"
	"time"

	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/types"
)

type Store interface {
	Get(ctx context.Context, tenantID, credentialIssuer string) (*Issuer, error)
	Insert(ctx context.Context, issuer Issuer) error
	Update(ctx context.Context, tenantID, credentialIssuer string, update IssuerUpdate) error
	List(ctx context.Context, tenantID string) ([]Issuer, error)
	ListAll(ctx context.Context) ([]Issuer, error)
}

type Issuer struct {
	CredentialIssuer                    string
	TenantID                            string
	AuthorizationServers                []string
	CredentialEndpoint                  string
	BatchCredentialEndpoint             *string
	DeferredCredentialEndpoint          *string
	CredRespEncryptAlgValuesSupported   []string
	CredRespEncryptEncValuesSupported   []string
	RequireCredentialResponseEncryption bool
	CredentialIdentifiersSupported      bool
	Display                             []types.LocalizedCredential
	CredentialsSupported                []CredentialsSupported
	FirstSeen                           time.Time
	LastSeen                            time.Time
}

type Locale string

type IssuerUpdate struct {
	AuthorizationServers                []string
	CredentialEndpoint                  *string
	BatchCredentialEndpoint             *string
	DeferredCredentialEndpoint          *string
	CredRespEncryptAlgValuesSupported   []string
	CredRespEncryptEncValuesSupported   []string
	RequireCredentialResponseEncryption *bool
	CredentialIdentifiersSupported      *bool
	Display                             []types.LocalizedCredential
	CredentialsSupported                []CredentialsSupported
	LastSeen                            *time.Time
}

type CredentialsSupported struct {
	ID                            string                      `json:"id"`
	Format                        string                      `json:"format"`
	Scope                         string                      `json:"scope"`
	CryptographicBindingsReported []string                    `json:"cryptographic_binding_methods_supported"`
	CryptographicSuitesReported   []string                    `json:"cryptographic_suites_supported"`
	CredentialDefinition          CredentialDefinition        `json:"credential_definition"`
	ProofTypesSupported           []string                    `json:"proof_types_supported"`
	Display                       []types.LocalizedCredential `json:"display"`
}

type CredentialDefinition struct {
	Type              []string `json:"type"`
	CredentialSubject map[string]CredentialSubject
}

type CredentialSubject struct {
	Display []types.LocalizedCredential `json:"display"`
}
