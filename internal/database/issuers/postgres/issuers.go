package postgres

import (
	"context"
	"fmt"
	"time"

	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/logr"

	"github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgxpool"

	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/database"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/database/issuers"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/database/postgres"
)

type Store struct {
	log logr.Logger
	db  *pgxpool.Pool
	sq  squirrel.StatementBuilderType
}

var _ issuers.Store = Store{}

const (
	colCredentialIssuer                    = "credential_issuer"
	colTenantId                            = "tenant_id"
	colAuthorizationServers                = "authorization_servers"
	colCredentialEndpoint                  = "credential_endpoint"
	colBatchCredentialEndpoint             = "batch_credential_endpoint"
	colDeferredCredentialEndpoint          = "deferred_credential_endpoint"
	colCredRespEncryptAlgValuesSupported   = "credential_response_encryption_alg_values_supported"
	colCredRespEncryptEncValuesSupported   = "credential_response_encryption_enc_values_supported"
	colRequireCredentialResponseEncryption = "require_credential_response_encryption"
	colCredentialIdentifiersSupported      = "credential_identifiers_supported"
	colDisplay                             = "display"
	colFirstSeen                           = "first_seen"
	colLastSeen                            = "last_seen"

	colId                                   = "id"
	colFormat                               = "format"
	colScope                                = "scope"
	colCryptographicBindingMethodsSupported = "cryptographic_binding_methods_supported"
	colCryptographicSuitesSupported         = "cryptographic_suites_supported"
	colCredentialDefinition                 = "credential_definition"
	colProofTypesSupported                  = "proof_types_supported"
	colSuitesReported                       = "suites_reported"
)

func NewStore(db *pgxpool.Pool, logger logr.Logger) Store {
	return Store{
		log: logger,
		db:  db,
		sq:  postgres.StmtBuilderDollar(),
	}
}
func (s Store) Get(ctx context.Context, tenantID, name string) (*issuers.Issuer, error) {
	rows, err := s.list(
		ctx,
		colCredentialIssuer,
		squirrel.Eq{postgres.Prepend(postgres.TblIssuers, colCredentialIssuer): name},
		squirrel.Eq{postgres.Prepend(postgres.TblIssuers, colTenantId): tenantID},
	)
	if err != nil {
		return nil, err
	}

	if len(rows) < 1 {
		return nil, database.ErrNotFound
	}

	return &rows[0], nil
}

func (s Store) List(ctx context.Context, tenantID string) ([]issuers.Issuer, error) {
	return s.list(ctx, colCredentialIssuer, squirrel.Eq{colTenantId: tenantID})
}

func (s Store) ListAll(ctx context.Context) ([]issuers.Issuer, error) {
	return s.list(ctx, colCredentialIssuer)
}

func (s Store) Insert(ctx context.Context, issuer issuers.Issuer) error {
	query := s.sq.
		Insert(postgres.TblIssuers).
		Columns(
			colCredentialIssuer, colTenantId,
			colAuthorizationServers, colCredentialEndpoint,
			colBatchCredentialEndpoint, colDeferredCredentialEndpoint,
			colCredRespEncryptAlgValuesSupported, colCredRespEncryptEncValuesSupported,
			colRequireCredentialResponseEncryption, colCredentialIdentifiersSupported,
			colDisplay, colFirstSeen, colLastSeen,
		).
		Values(
			issuer.CredentialIssuer, issuer.TenantID,
			issuer.AuthorizationServers, issuer.CredentialEndpoint,
			issuer.BatchCredentialEndpoint, issuer.DeferredCredentialEndpoint,
			issuer.CredRespEncryptAlgValuesSupported, issuer.CredRespEncryptEncValuesSupported,
			issuer.RequireCredentialResponseEncryption, issuer.CredentialIdentifiersSupported,
			issuer.Display, issuer.FirstSeen, issuer.LastSeen,
		)

	sql, params, err := query.ToSql()
	if err != nil {
		return database.NewError("failed to build query", err)
	}

	if _, err := s.db.Exec(ctx, sql, params...); err != nil {
		return database.NewError("failed to execute query", err)
	}

	return s.insertCredentialsSupported(ctx, issuer.TenantID, issuer.CredentialIssuer, issuer.CredentialsSupported)
}

func (s Store) insertCredentialsSupported(ctx context.Context, tenantID, issuer string, cs []issuers.CredentialsSupported) error {
	query := s.sq.
		Insert(postgres.TblCredentialsSupported).
		Columns(
			colTenantId, colCredentialIssuer, colId, colFormat, colScope,
			colCryptographicBindingMethodsSupported, colCryptographicSuitesSupported,
			colCredentialDefinition, colProofTypesSupported,
			colFirstSeen, colLastSeen,
		)

	for _, supported := range cs {
		query = query.Values(
			tenantID, issuer, supported.ID, supported.Format, supported.Scope,
			supported.CryptographicBindingsReported, supported.CryptographicSuitesReported,
			supported.CredentialDefinition, supported.ProofTypesSupported,
			time.Now(), time.Now(),
		)
	}

	sql, params, err := query.ToSql()
	if err != nil {
		return database.NewError("failed to build query", err)
	}

	if _, err := s.db.Exec(ctx, sql, params...); err != nil {
		return database.NewError("failed to insert credentials supported", err)
	}

	return nil
}

func (s Store) Update(ctx context.Context, tenantID, issuer string, update issuers.IssuerUpdate) error {
	query := s.sq.
		Update(postgres.TblIssuers).
		Where(squirrel.Eq{colCredentialIssuer: issuer}).
		Where(squirrel.Eq{colTenantId: tenantID})

	if update.CredentialEndpoint != nil {
		query = query.Set(colCredentialEndpoint, update.CredentialEndpoint)
	}

	if update.AuthorizationServers != nil {
		query = query.Set(colAuthorizationServers, update.AuthorizationServers)
	}

	if update.LastSeen != nil {
		query = query.Set(colLastSeen, update.LastSeen)
	}

	sql, params, err := query.ToSql()
	if err != nil {
		return database.NewError("failed to build query", err)
	}

	if _, err := s.db.Exec(ctx, sql, params...); err != nil {
		return database.NewError("failed to execute query", err)
	}

	return nil
}

func (s Store) updateCredentialsSupported(ctx context.Context, tenantID, issuer string, update issuers.IssuerUpdate) error {
	query := s.sq.Delete(postgres.TblCredentialsSupported).Where(squirrel.Eq{colCredentialIssuer: issuer})

	sql, params, err := query.ToSql()
	if err != nil {
		return database.NewError("failed to build query", err)
	}

	if _, err := s.db.Exec(ctx, sql, params...); err != nil {
		return database.NewError("failed to update credentials supported", err)
	}

	return s.insertCredentialsSupported(ctx, tenantID, issuer, update.CredentialsSupported)
}

func (s Store) list(ctx context.Context, orderBy string, where ...any) ([]issuers.Issuer, error) {
	columns := postgres.PrependAll(postgres.TblIssuers,
		colCredentialIssuer, colTenantId,
		colAuthorizationServers, colCredentialEndpoint,
		colBatchCredentialEndpoint, colDeferredCredentialEndpoint,
		colCredRespEncryptAlgValuesSupported, colCredRespEncryptEncValuesSupported,
		colRequireCredentialResponseEncryption, colCredentialIdentifiersSupported,
		colFirstSeen, colLastSeen,
	)

	columns = append(columns, postgres.PrependAll(postgres.TblCredentialsSupported,
		colId, colFormat, colScope,
		colCryptographicBindingMethodsSupported, colCryptographicSuitesSupported,
		colCredentialDefinition, colProofTypesSupported,
	)...)

	query := s.sq.
		Select(columns...).
		From(postgres.TblIssuers).
		LeftJoin(fmt.Sprintf(
			"%s ON %s.%s=%s.%s",
			postgres.TblCredentialsSupported,
			postgres.TblIssuers, colCredentialIssuer,
			postgres.TblCredentialsSupported, colCredentialIssuer,
		)).
		OrderBy(postgres.Prepend(postgres.TblIssuers, colCredentialIssuer)).
		OrderBy(postgres.Prepend(postgres.TblIssuers, orderBy))

	for _, wh := range where {
		query = query.Where(wh)
	}

	sql, params, err := query.ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := s.db.Query(ctx, sql, params...)
	if err != nil {
		return nil, err
	}

	var out []issuers.Issuer
	var previous *issuers.Issuer
	for rows.Next() {
		var issuer issuers.Issuer
		var cs issuers.CredentialsSupported

		err := rows.Scan(
			&issuer.CredentialIssuer, &issuer.TenantID,
			&issuer.AuthorizationServers, &issuer.CredentialEndpoint,
			&issuer.BatchCredentialEndpoint, &issuer.DeferredCredentialEndpoint,
			&issuer.CredRespEncryptAlgValuesSupported, &issuer.CredRespEncryptEncValuesSupported,
			&issuer.RequireCredentialResponseEncryption, &issuer.CredentialIdentifiersSupported,
			&issuer.FirstSeen, &issuer.LastSeen,
			&cs.ID, &cs.Format, &cs.Scope,
			&cs.CryptographicBindingsReported, &cs.CryptographicSuitesReported,
			&cs.CredentialDefinition, &cs.ProofTypesSupported,
		)
		if err != nil {
			s.log.Error(err, "failed to scan")
			return nil, err
		}

		issuer.CredentialsSupported = []issuers.CredentialsSupported{cs}

		// first row
		if previous == nil {
			previous = &issuer
			continue
		}

		// new issuer
		if previous.CredentialIssuer != issuer.CredentialIssuer {
			out = append(out, *previous)
			previous = &issuer
			continue
		}

		// same issuer as before, just append new cs
		previous.CredentialsSupported = append(previous.CredentialsSupported, cs)
	}

	if previous != nil {
		// always append the last issuer
		out = append(out, *previous)
	}

	return out, nil
}
