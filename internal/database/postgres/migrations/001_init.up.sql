CREATE TABLE issuers (
    credential_issuer text NOT NULL,
    tenant_id text NOT NULL,
    authorization_servers text[],
    credential_endpoint text NOT NULL,
    batch_credential_endpoint text,
    deferred_credential_endpoint text,
    credential_response_encryption_alg_values_supported text[],
    credential_response_encryption_enc_values_supported text[],
    require_credential_response_encryption bool NOT NULL,
    credential_identifiers_supported bool NOT NULL DEFAULT false,
    display jsonb,
    first_seen timestamp with time zone,
    last_seen timestamp with time zone
);

ALTER TABLE issuers ADD PRIMARY KEY (tenant_id, credential_issuer);

CREATE TABLE credentials_supported (
    tenant_id text NOT NULL,
    credential_issuer text NOT NULL,
    id text NOT NULL,
    format text NOT NULL,
    scope text,
    cryptographic_binding_methods_supported text[],
    cryptographic_suites_supported text[],
    credential_definition jsonb,
    proof_types_supported text[],
    display jsonb,
    first_seen timestamp with time zone,
    last_seen timestamp with time zone
);

ALTER TABLE credentials_supported ADD PRIMARY KEY (tenant_id, credential_issuer, id, format);
CREATE INDEX idx_format ON credentials_supported (tenant_id, format);
CREATE INDEX idx_id ON credentials_supported (tenant_id, id);