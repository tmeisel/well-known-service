package api

import (
	"github.com/gin-gonic/gin"
	serverPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/server"

	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/handlers"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/importer"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/model"
)

func WellKnown(server *serverPkg.Server, imp importer.Importer, env *common.Environment) {
	config := env.GetConfig()
	server.Add(func(rg *gin.RouterGroup) {
		g := rg.Group("/.well-known")
		g.GET("/openid-credential-issuer", func(c *gin.Context) {
			handlers.WellKnownCredentialIssuerHandler(c, imp, env)
		})
		g.GET("/jwt-issuer", func(c *gin.Context) {
			location := c.GetHeader(config.LocationHeader)
			jwksurl := c.GetHeader(config.JwtIssuer.JwksUrlHeader)
			if location == "" {
				response := new(model.Config)
				if config.JwtIssuer.Issuer == "" {
					response.JwtIssuer.Issuer = location
				} else {
					response.JwtIssuer.Issuer = config.JwtIssuer.Issuer
				}

				if config.JwtIssuer.JwksUrlHeader == "" {
					response.JwtIssuer.JwksUrlHeader = jwksurl
				} else {
					response.JwtIssuer.JwksUrlHeader = config.JwtIssuer.JwksUrlHeader
				}

				c.JSON(200, config.JwtIssuer)

			} else {
				c.JSON(400, "")
			}
		})
	})
}
