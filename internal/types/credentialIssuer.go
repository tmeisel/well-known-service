package types

type CredentialIssuer struct {
	CredentialIssuer                    string
	TenantID                            string
	AuthorizationServers                []string
	CredentialEndpoint                  string
	BatchCredentialEndpoint             *string
	DeferredCredentialEndpoint          *string
	CredRespEncryptAlgValuesSupported   []string
	CredRespEncryptEncValuesSupported   []string
	RequireCredentialResponseEncryption *bool
	CredentialIdentifiersSupported      bool
	Display                             []LocalizedCredential
	CredentialsSupported                []CredentialsSupported
}

type CredentialsSupported struct {
	ID                            string                `json:"id"`
	Format                        string                `json:"format"`
	Scope                         string                `json:"scope"`
	CryptographicBindingsReported []string              `json:"cryptographic_binding_methods_supported"`
	CryptographicSuitesReported   []string              `json:"cryptographic_suites_supported"`
	CredentialDefinition          string                `json:"credential_definition"`
	ProofTypesSupported           []string              `json:"proof_types_supported"`
	Display                       []LocalizedCredential `json:"display"`
}

type LocalizedCredential struct {
	Name            string         `json:"name"`
	Locale          string         `json:"locale"`
	Logo            DescriptiveURL `json:"logo,omitempty"`
	BackgroundColor string         `json:"background_color,omitempty"`
	TextColor       string         `json:"text_color,omitempty"`
}

type DescriptiveURL struct {
	URL             string `json:"url"`
	AlternativeText string `json:"alternative_text"`
}
