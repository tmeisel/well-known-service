package importer

import (
	"context"
	"errors"

	serverPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/server"

	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/types"
)

var ErrNotFound = errors.New("not found")

type Importer interface {
	Start(server *serverPkg.Server, env *common.Environment) error
	Stop() error
	GotErrors() bool
	GetCredentialIssuerMetadata(ctx context.Context, templateData types.TemplateData, env *common.Environment) (*types.CredentialIssuer, error)
}
