package importer

import (
	"bytes"
	"context"
	"encoding/json"
	"html/template"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/madflojo/tasks"
	serverPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/server"
	"gopkg.in/src-d/go-git.v4"

	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/types"
)

type GitImporter struct {
	taskScheduler *tasks.Scheduler
	folder        string
	repo          *git.Repository
	lastError     error
}

func (g *GitImporter) Start(server *serverPkg.Server, env *common.Environment) error {
	config := env.GetConfig()
	logger := env.GetLogger()
	g.taskScheduler = tasks.New()
	g.folder = os.TempDir() + string(os.PathSeparator) + "cache"

	server.Add(func(rg *gin.RouterGroup) {
		rg.Static(config.Git.ImagePath, g.folder)
	})

	_, err := g.taskScheduler.Add(&tasks.Task{

		Interval: time.Second * time.Duration(config.Git.Interval),
		TaskFunc: func() error {

			logger.Info("git clone " + g.folder)
			repo := config.Git.Repo
			token := config.Git.Token

			cloneOpt := git.CloneOptions{
				URL:      repo,
				Progress: os.Stdout,
			}

			if token != "" {
				cloneOpt.URL = strings.Replace(cloneOpt.URL, "https://", "https://token:"+token+"@", 1)
			}

			var err error
			var gitRepo *git.Repository
			if g.repo != nil {
				w, err := g.repo.Worktree()

				if err == nil {
					err = w.Pull(&git.PullOptions{RemoteName: "origin"})
					if err == nil {
						ref, err := g.repo.Head()
						if err == nil {
							commit, err := g.repo.CommitObject(ref.Hash())

							if err == nil {
								logger.Info("Pulled " + commit.String())
							}
						}
					}
				}
			} else {
				gitRepo, err = git.PlainClone(g.folder, false, &cloneOpt)
				if err == nil {
					g.repo = gitRepo
				}
			}
			return err
		},
	})

	if err != nil {
		logger.Error(err, "Can create scheduler for git importer")
	}

	return nil
}

func (g *GitImporter) Stop() error {
	g.taskScheduler.Stop()
	return nil
}

func (g *GitImporter) GotErrors() bool {
	return g.lastError != nil
}

func (g *GitImporter) GetCredentialIssuerMetadata(_ context.Context, templateData types.TemplateData, env *common.Environment) (*types.CredentialIssuer, error) {
	logger := env.GetLogger()

	issuerData, err := os.ReadFile(strings.Join([]string{g.folder, templateData.TenantId, "issuer.json"}, string(os.PathSeparator)))
	if err != nil {
		logger.Error(err, "failed to read file from disk")
		return nil, err
	}

	var credentialIssuer types.CredentialIssuer
	credentialIssuer.CredentialsSupported = g.CollectCredentials(templateData, env)

	t, err := template.New("replace").Parse(string(issuerData))
	if err != nil {
		logger.Error(err, "Error during string replacement.")
		return nil, err
	}

	var buf bytes.Buffer
	if err = t.Execute(&buf, templateData); err != nil {
		logger.Error(err, "failed to execute template", "buffer", buf.String())
		return nil, err
	}

	if err = json.Unmarshal(buf.Bytes(), &credentialIssuer); err != nil {
		logger.Error(err, "failed to unmarshal credentialIssuer")
		return nil, err
	}

	return &credentialIssuer, nil

}

func (g *GitImporter) CollectCredentials(templateData types.TemplateData, env *common.Environment) []types.CredentialsSupported {
	logger := env.GetLogger()
	directory := strings.Join([]string{g.folder, templateData.TenantId, "credentials"}, string(os.PathSeparator))
	files, err := os.ReadDir(directory)
	if err != nil {
		logger.Error(err, "Error reading Directory")
		return nil
	}

	credentials := make([]types.CredentialsSupported, 0)

	for _, file := range files {
		if !file.IsDir() {
			credential, err := os.ReadFile(strings.Join([]string{directory, file.Name()}, string(os.PathSeparator)))

			if err == nil {
				t, err := template.New("replace").Parse(string(credential))
				if err != nil {
					logger.Error(err, "Error during string Replacement")
					return nil
				}
				var buf bytes.Buffer
				err = t.Execute(&buf, templateData)
				logger.Debug(buf.String())
				if err == nil {
					var cred types.CredentialsSupported
					err := json.Unmarshal(buf.Bytes(), &cred)
					if err == nil {
						credentials = append(credentials, cred)
					}
				}
			}
		}
	}
	return credentials
}
