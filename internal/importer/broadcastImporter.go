package importer

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/database"

	"github.com/cloudevents/sdk-go/v2/event"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	coreTypes "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/logr"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/server"

	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/database/issuers"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/types"
)

type BroadcastImporter struct {
	stopChan chan bool

	issuerStore issuers.Store
	ceProvider  *cloudeventprovider.CloudEventProviderClient
	log         *coreTypes.Logger
}

var _ Importer = &BroadcastImporter{}

const (
	writeInterval = time.Second * 30
)
const (
	eventTypeIssuer = "issuer"
)

const (
	UniversityIssuer = "university-issuer"
	CitizenIssuer    = "citizen-issuer"
)

func NewBroadcastImporter(store issuers.Store, ceProvider *cloudeventprovider.CloudEventProviderClient, logger *coreTypes.Logger) *BroadcastImporter {
	return &BroadcastImporter{
		stopChan:    make(chan bool),
		issuerStore: store,
		ceProvider:  ceProvider,
		log:         logger,
	}
}

func (b *BroadcastImporter) Start(_ *server.Server, _ *common.Environment) error {
	go b.listen()

	return nil
}

func (b *BroadcastImporter) Stop() error {
	b.stopChan <- true
	return nil
}

func (b *BroadcastImporter) GotErrors() bool {
	return false
}

func (b *BroadcastImporter) GetCredentialIssuerMetadata(ctx context.Context, templateData types.TemplateData, env *common.Environment) (*types.CredentialIssuer, error) {
	issuer, err := b.issuerStore.Get(ctx, templateData.TenantId, UniversityIssuer)
	if err != nil {
		return nil, ErrNotFound
	}

	cs := make([]types.CredentialsSupported, 0)
	for _, supported := range issuer.CredentialsSupported {
		cs = append(cs, types.CredentialsSupported{
			ID:                            supported.ID,
			Format:                        supported.Format,
			Scope:                         supported.Scope,
			CryptographicBindingsReported: supported.CryptographicBindingsReported,
			CryptographicSuitesReported:   supported.CryptographicSuitesReported,
			ProofTypesSupported:           supported.ProofTypesSupported,
			Display:                       supported.Display,
		})
	}

	return &types.CredentialIssuer{
		CredentialIssuer:                    issuer.CredentialIssuer,
		CredentialEndpoint:                  issuer.CredentialEndpoint,
		AuthorizationServers:                issuer.AuthorizationServers,
		BatchCredentialEndpoint:             issuer.BatchCredentialEndpoint,
		DeferredCredentialEndpoint:          issuer.DeferredCredentialEndpoint,
		CredRespEncryptAlgValuesSupported:   issuer.CredRespEncryptAlgValuesSupported,
		CredRespEncryptEncValuesSupported:   issuer.CredRespEncryptEncValuesSupported,
		RequireCredentialResponseEncryption: &issuer.RequireCredentialResponseEncryption,
		CredentialIdentifiersSupported:      issuer.CredentialIdentifiersSupported,
		CredentialsSupported:                cs,
		Display:                             nil,
	}, nil
}

func (b *BroadcastImporter) listen() {
	for {
		// TODO: this function cannot be cancelled (ceProvider.Sub does not even take a context)
		if err := b.ceProvider.Sub(b.handleEvent); err != nil {
			b.log.Error(err, "cloudEventProvider.Sub failed")
		}
	}
}

// TODO: define events?!
func (b *BroadcastImporter) handleEvent(e event.Event) {
	switch e.Type() {
	case eventTypeIssuer:
		go b.handleIssuerEvent(context.TODO(), e.Data())
	}
}

func (b *BroadcastImporter) handleIssuerEvent(ctx context.Context, data []byte) {
	var issuer types.CredentialIssuer
	if err := json.Unmarshal(data, &issuer); err != nil {
		b.log.Error(err, "failed to unmarshal issuer")
	}

	if issuer.TenantID == "" {
		b.log.Info("invalid issuer (empty tenantID)", "issuer", issuer)
	}

	storedIssuer, err := b.issuerStore.Get(ctx, issuer.TenantID, issuer.CredentialIssuer)
	if err != nil && !errors.Is(err, database.ErrNotFound) {
		b.log.Error(err, "failed to get storedIssuer")
		return
	}

	now := time.Now()
	isNew := storedIssuer == nil

	if isNew {
		if issuer.RequireCredentialResponseEncryption == nil {
			issuer.RequireCredentialResponseEncryption = types.BoolPtr(false)
		}

		cs := make([]issuers.CredentialsSupported, 0)
		for _, supported := range issuer.CredentialsSupported {
			cs = append(cs, issuers.CredentialsSupported{
				ID:                            supported.ID,
				Format:                        supported.Format,
				Scope:                         supported.Scope,
				CryptographicBindingsReported: supported.CryptographicBindingsReported,
				CryptographicSuitesReported:   supported.CryptographicSuitesReported,
				ProofTypesSupported:           supported.ProofTypesSupported,
				Display:                       supported.Display,
			})
		}

		storedIssuer = &issuers.Issuer{
			CredentialIssuer:                    issuer.CredentialIssuer,
			TenantID:                            issuer.TenantID,
			AuthorizationServers:                issuer.AuthorizationServers,
			CredentialEndpoint:                  issuer.CredentialEndpoint,
			BatchCredentialEndpoint:             issuer.BatchCredentialEndpoint,
			DeferredCredentialEndpoint:          issuer.DeferredCredentialEndpoint,
			CredRespEncryptAlgValuesSupported:   issuer.CredRespEncryptAlgValuesSupported,
			CredRespEncryptEncValuesSupported:   issuer.CredRespEncryptEncValuesSupported,
			RequireCredentialResponseEncryption: *issuer.RequireCredentialResponseEncryption,
			CredentialsSupported:                cs,
			LastSeen:                            now,
			FirstSeen:                           now,
		}

		if err := b.issuerStore.Insert(ctx, *storedIssuer); err != nil {
			b.log.Error(err, "failed to insert new issuer", "prev", errors.Unwrap(err))
			return
		}

		return
	}

	update := issuers.IssuerUpdate{
		AuthorizationServers:                issuer.AuthorizationServers,
		CredentialEndpoint:                  &issuer.CredentialEndpoint,
		BatchCredentialEndpoint:             issuer.BatchCredentialEndpoint,
		DeferredCredentialEndpoint:          issuer.DeferredCredentialEndpoint,
		CredRespEncryptAlgValuesSupported:   issuer.CredRespEncryptAlgValuesSupported,
		CredRespEncryptEncValuesSupported:   issuer.CredRespEncryptEncValuesSupported,
		RequireCredentialResponseEncryption: issuer.RequireCredentialResponseEncryption,
		LastSeen:                            &now,
	}

	if err := b.issuerStore.Update(ctx, issuer.TenantID, issuer.CredentialIssuer, update); err != nil {
		b.log.Error(err, "failed to update existing issuer")
		return
	}
}
