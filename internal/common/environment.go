package common

import (
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/logr"

	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/model"
)

type Environment struct {
	logger     *logr.Logger
	config     *model.Config
	healthFunc func() bool
}

var env *Environment

func init() {
	env = new(Environment)
}

func GetEnvironment() *Environment {
	return env
}

func (e *Environment) IsHealthy() bool {
	return e.healthFunc()
}

func (e *Environment) SetHealthFunc(healthFunc func() bool) {
	e.healthFunc = healthFunc
}

func (e *Environment) SetConfig(config *model.Config) {
	e.config = config
}

func (e *Environment) GetConfig() *model.Config {
	return e.config
}

func (e *Environment) SetLogger(logger *logr.Logger) {
	e.logger = logger
}

func (e *Environment) GetLogger() *logr.Logger {
	return e.logger
}
