package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	configPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/config"
	postgresPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/db/postgres"
	errPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/err"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/logr"
	serverPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/server"
	"golang.org/x/sync/errgroup"

	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/api"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/common"
	issuer "gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/database/issuers/postgres"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/database/postgres"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/importer"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/well-known-service/internal/model"
)

var env *common.Environment

func main() {
	_, ctx := errgroup.WithContext(context.Background())

	env = common.GetEnvironment()
	conf := new(model.Config)

	if err := configPkg.LoadConfig("WELLKNOWN", &conf, nil); err != nil {
		log.Fatalf("failed to load config: %t", err)
	}

	logger, err := logr.New(conf.LogLevel, true, nil)
	if err != nil {
		log.Fatalf("failed to init logger: %t", err)
	}

	env.SetLogger(logger)
	env.SetConfig(conf)

	errChan := make(chan error)
	go errPkg.LogChan(*logger, errChan)
	pgDb, err := postgresPkg.ConnectRetry(ctx, conf.Postgres, time.Minute, errChan)
	if err != nil {
		logger.Error(err, "failed to connect to postgres")
		os.Exit(1)
	}

	if err := postgresPkg.MigrateUP(pgDb, postgres.Migrations, "migrations"); err != nil {
		logger.Error(err, fmt.Sprintf("migration failed: %s", err))
		os.Exit(1)
	}

	const topic = "test"
	ceProvider, err := cloudeventprovider.NewClient(cloudeventprovider.Sub, topic)
	if err != nil {
		logger.Error(err, "failed to connect to nats serverPkg")
		os.Exit(1)
	}

	var imp importer.Importer
	switch conf.CredentialIssuer.Importer {
	case "GIT":
		imp = new(importer.GitImporter)
	case "BROADCAST":
		imp = importer.NewBroadcastImporter(issuer.NewStore(pgDb, *logger), ceProvider, logger)
	}

	env.SetHealthFunc(imp.GotErrors)

	server := serverPkg.New(env)
	if err := imp.Start(server, env); err != nil {
		logger.Error(err, "Importer cant be started")
	}

	defer imp.Stop()

	api.WellKnown(server, imp, env)

	logger.Debug("Start Server")

	if err := server.Run(conf.Port); err != nil {
		logger.Error(err, "Server couldn't start.")
	}
}
