# Well Known Service

## Folder Structure

The folders must contain the following structure: 

```

-tenantID
  - images
  - credentials
  - issuer.json
``` 

The folder images can contain images like png. Credentials must contain json.

## Issuer.json

Must consist of a json file with the following structure (templates will be replaced e.g. {{origin by https://xyz}}): 

```
   "CredentialIssuer": "{{.Origin}}"
   "CredentialEndpont": "{{.Origin}}/credentials"
   "AuthorizationServers": []
   ...
   "CredentialsSupported":"{{.Credentials}}"
```